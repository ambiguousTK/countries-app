import React from 'react'
import {func} from 'prop-types';


import moonIcon from '../../assets/svg/moon.svg';
import sunIcon from '../../assets/svg/sun.svg';
import {useObserver} from "mobx-react";
import {StoreContext} from "../../App";

require("./toggle.scss");

const Toggle = ({toggleTheme}) => {
    const store = React.useContext(StoreContext);

    return useObserver(() => (
        <span className="toggle" onClick={toggleTheme}>
            {
                store.theme === 'light' &&
                <span>
                    <img src={moonIcon} alt="moonIcon"/> Dark mode
                </span>
            }
            {
                store.theme !== 'light' &&
                <span>
                        <img src={sunIcon} alt="sunIcon"/>  Light mode
                </span>
            }
        </span>
    ));
};

Toggle.propTypes = {
    toggleTheme: func.isRequired,
};

export default Toggle;
