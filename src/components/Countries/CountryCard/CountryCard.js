import React from "react";
import {Card} from "react-bootstrap/esm/index";
import { useHistory } from "react-router-dom";

require("./countryCard.scss");

function CountryCard(props) {
    const history = useHistory();

    const {country} = props;
    return (
            <Card onClick={()=>history.push(`/country/${country.name}`)}>
                <Card.Img variant="top" src={country.flag}/>
                <Card.Body>
                    <Card.Title>{country.name}</Card.Title>
                    <Card.Text as='div'>
                        <div><strong>population: </strong>{country.population.toLocaleString()}</div>
                        <div><strong>Region: </strong>{country.region}</div>
                        <div><strong>Capital: </strong>{country.capital}</div>
                    </Card.Text>
                </Card.Body>
            </Card>
    );
}

export default CountryCard;
