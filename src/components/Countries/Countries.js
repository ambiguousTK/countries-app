import React, {useEffect, useState} from "react";
import ActionBar from "./ActionBar/ActionBar";
import {Col, Row} from "react-bootstrap";
import CountryGrid from "./CountryGrid/CountryGrid";
import {useObserver} from "mobx-react";
import {StoreContext} from "../../App";
import CountryTable from "./CountryTable/CountryTable";
import LoadingComponent from "../LoadingComponent/LoadingComponent";

const COUNTRIES_SERVICE_URL = "https://restcountries.eu/rest/v2/all";

const Countries = () => {
    const store = React.useContext(StoreContext);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, []);

    const [isBottom, setIsBottom] = useState(false);

    function handleScroll() {
        const scrollTop = (document.documentElement
            && document.documentElement.scrollTop)
            || document.body.scrollTop;
        const scrollHeight = (document.documentElement
            && document.documentElement.scrollHeight)
            || document.body.scrollHeight;
        if (scrollTop + window.innerHeight + 50 >= scrollHeight) {
            setIsBottom(true);
        }
    }

    useEffect(() => {
        if (isBottom) {
            addCountries();
        }
    }, [isBottom]);


    const addCountries = () => {
        if (countries.length !== 0) {
            const newCountriesSubList = countries.slice(countriesSubList.length, countriesSubList.length + 40);
            const concatArray = countriesSubList.concat(newCountriesSubList);
            setCountriesSubList([...concatArray]);
        }
        setIsBottom(false);
    };
    const [isFetching, setFetching] = useState(false);
    const [countries, setCountries] = useState([]);
    const [countriesSubList, setCountriesSubList] = useState([]);

    useEffect(() => {
        fetchCountries();
    }, []);

    const fetchCountries = () => {
        setFetching(true);
        fetch(COUNTRIES_SERVICE_URL).then(
            response => response.json()
        ).then(result => {
            if (result && result.length > 0) {
                setFetching(false);
                setCountries(result);
                setCountriesSubList(result.slice(0, 20));
            }
        }).catch(e => {
            console.log(e);
            setFetching(false);
        });
    };

    return useObserver(() => {
        let renderedCountriesList = [];
        if (store.regionSearchValue && store.regionSearchValue.length > 0 || store.countrySearchValue && store.countrySearchValue.length > 0) {
            let filteredCountries = [...countries];
            if (store.regionSearchValue && store.regionSearchValue.length > 0) {
                filteredCountries = countries.filter(c => c.region.toLowerCase().includes(store.regionSearchValue.toLowerCase()));
            }
            if (store.countrySearchValue && store.countrySearchValue.length > 0) {
                filteredCountries = filteredCountries.filter(c => c.name.toLowerCase().includes(store.countrySearchValue.toLowerCase()));
            }
            renderedCountriesList = [...filteredCountries];
        } else {
            renderedCountriesList = [...countriesSubList];
        }
        return (
            <div>
                <ActionBar/>
                <Row className="mt-2">
                    <Col lg={12}>
                        {isFetching && <div className="loading-component-container"><LoadingComponent /></div>}
                        {!isFetching && store.countriesViewMode === "card" && <CountryGrid countries={renderedCountriesList}/>}
                        {!isFetching && store.countriesViewMode === "table" && <CountryTable countries={renderedCountriesList}/>}

                    </Col>
                </Row>
            </div>
        )
    });
};

export default Countries;
