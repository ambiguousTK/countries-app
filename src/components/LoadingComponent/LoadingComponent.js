import React from "react";

const LoadingComponent = (props) => {
    return (
        <div className={`spinner-grow spinner-grow-${props.size}`} role="status">
            <span className="sr-only">Loading...</span>
        </div>
    );
};

export default LoadingComponent;