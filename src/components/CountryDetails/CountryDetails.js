import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {StoreContext} from "../../App";
import {Alert, Row, Col, Container} from "react-bootstrap";
import LoadingComponent from "../LoadingComponent/LoadingComponent";
import { useHistory } from "react-router-dom";

require("./countryDetails.scss");

const COUNTRY_SEARCH_BY_NAME_SERVICE_URL = "https://restcountries.eu/rest/v2/name";
const COUNTRY_SEARCY_BY_CODES_SERVICE_URL = "https://restcountries.eu/rest/v2/alpha";

const CountryDetails = () => {
    const {name} = useParams();
    const [isFetching, setFetching] = useState(false);
    const [country, setCountry] = useState(null);

    const [isBordersCountriesFetching, setBordersCountriesFetching] = useState(false);
    const [bordersCountries, setBordersCountries] = useState([]);
    const [bordersCountriesNames, setBordersCountriesNames] = useState([]);

    const store = React.useContext(StoreContext);
    const history = useHistory();


    const fetchCountry = (countryName) => {
        setFetching(true);
        fetch(`${COUNTRY_SEARCH_BY_NAME_SERVICE_URL}/${countryName}?fullText=true`).then(
            response => response.json()
        ).then(result => {
            setFetching(false);
            if (result.status === 404) {
                setCountry("");
            }
            if (result && result.length > 0) {
                setCountry(result[0]);
                setBordersCountries(result[0].borders);
            }
        }).catch(e => {
            setFetching(false);
            setCountry("");
        });
    };

    useEffect(()=>{
        fetchCountry(name);
        store.setMeta({title: "Country Details"});
        return () => {
            store.setMeta({title: ""});
        }
    }, []);
    useEffect(()=>{
        if (bordersCountries.length === 0) return;
        const codes = bordersCountries.join(';');
        const url = `${COUNTRY_SEARCY_BY_CODES_SERVICE_URL}?codes=${codes}`;
        setBordersCountriesFetching(true);
        fetch(url).then(
            response => response.json()
        ).then(result => {
            setBordersCountriesFetching(false);
            if (result.status === 404) {
                setBordersCountriesNames([]);
            }
            if (result && result.length > 0) {
                const borderNames = [];
                result.forEach(r=>{
                    borderNames.push(r.name);
                });
                setBordersCountriesNames([...borderNames]);

            }
        }).catch(e => {
            setBordersCountriesFetching(false);
            setBordersCountriesNames([]);
        });
    }, [bordersCountries]);

    const countryDetailsMarkup = () => {

        return (
            <Container>
                <Row>
                    <Col lg={2}>
                        <button id="back-button" onClick={()=>history.push(`/`)}>
                            <div id="left-arrow"/>
                            Back
                        </button>
                    </Col>
                </Row>
                <Row className="mt-3">
                    <Col lg={5} className="mr-lg-4">
                        <img src={country.flag} alt={country.name} className="details-flag"/>
                    </Col>
                    <Col lg={6}>
                        <Row className="mobile-space">
                            <Col><h1 className="country-name">{country.name}</h1></Col>
                        </Row>
                        <Row className="mb-lg-4">
                            <Col lg={6} id="details-1">
                                <div><strong>Native Name: </strong>{country.nativeName}</div>
                                <div><strong>Population: </strong>{country.population.toLocaleString()}</div>
                                <div><strong>Region: </strong>{country.region}</div>
                                <div><strong>Sub Region: </strong>{country.subregion}</div>
                                <div><strong>Capital: </strong>{country.capital}</div>
                            </Col>
                            <Col id="details-2">
                                <div><strong>Top Level Domain: </strong>{country.topLevelDomain[0]}</div>
                                <div><strong>Currencies: </strong>{country.currencies.map((c,index)=>index === country.currencies.length-1 ? c.name : <span key={`${c.name}-${index}`}>{c.name},</span>)}</div>
                                <div><strong>Languages: </strong>{country.languages.map((l,index)=>index === country.languages.length-1 ? l.name : <span key={`${l.name}-${index}`}>{l.name},</span>)}</div>
                            </Col>
                        </Row>
                        <Row className="mt-xs-4 mt-sm-4 mt-md-4">
                            <Col id="details-3">
                                <div className="border-countries">
                                    <strong>Border Countries: </strong>
                                    {
                                        isBordersCountriesFetching ? <LoadingComponent size="sm"/> :
                                            bordersCountriesNames.map((b, index)=>
                                                <span key={`${b}-${index}`} className="country-boundry">{b}</span>
                                            )
                                    }
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        );
    };

    return (
        <Row className="mt-3">
            <Col>
                { isFetching && <div className="loading-component-container"><LoadingComponent/></div>}
                {!isFetching && country === "" && <Alert variant="danger">Not found</Alert>}
                {!isFetching && (country != null) && (typeof country === "object") && countryDetailsMarkup()}
            </Col>
        </Row>
    );
};

export default CountryDetails;
