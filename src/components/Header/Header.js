import React from "react";
import {Col, Container, Row} from "react-bootstrap";
import Toggle from "../Toggle/Toggle";
import {StoreContext} from "../../App";
import {useObserver} from "mobx-react";

require("./header.scss");



const Header = () => {
    const store = React.useContext(StoreContext);

    return useObserver(() => (
        <header>
            <Container style={{height: "100%"}}>
                <Row style={{height: "100%"}}>
                    <Col lg={4} className="d-none d-lg-block">
                        <h2>{store.meta.title}</h2>
                    </Col>
                    <Col lg={{span: 2, offset: 6}} xs={{span:5, offset: 7}} style={{textAlign: "right"}}>
                        <Toggle toggleTheme={store.toggleTheme}/>
                    </Col>
                </Row>
            </Container>
        </header>
    ));
};
export default Header;
