import React, { useRef, useEffect } from "react";
import {useObserver} from "mobx-react";
import {StoreContext} from "../../App";

require("./switchView.scss");

const SwitchView = () => {
    const store = React.useContext(StoreContext);

    const inputRef = useRef(null);

    const  _handleChange = (e) => {
        const mode = e.target.checked ? "card" : "table";
        store.setCountriesViewMode(mode);

    }
    useEffect(()=>{

    }, []);
    return useObserver(() => (
        <div className="switch-container">
            <strong>Table</strong>
            <label className="switch-label">
                <input ref={inputRef} checked={ store.countriesViewMode === "card" } onChange={ (e)=>_handleChange(e) } className="switch" type="checkbox" />
                <div>
            
                    <div></div>
                </div>
            </label>
            <strong>Card</strong>
        </div>
    ));
}

export default SwitchView;