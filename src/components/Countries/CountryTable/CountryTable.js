import React, { useEffect } from "react";
import {Table} from "react-bootstrap/esm/index";
import {StoreContext} from "../../../App";
import { useHistory } from "react-router-dom";

require("./countryTable.scss");

const CountryTable = (props) => {
    const store = React.useContext(StoreContext);
    const history = useHistory();

    const tableHeaders = ["Flag", "Population", "Region", "Capital", "Country", "Currency"];
    useEffect(()=>{
        store.setMeta({title: "Countries Table View"});
        return () => {
        store.setMeta({title: ""});

        }
    },[]);
    const handleRowClick = (countryName) => {
        history.push(`/country/${countryName}`);
    };
    return (
        <Table responsive>
            <thead>
                <tr>
                {tableHeaders.map((_, index) => (
                    <th key={index} className={_.toLowerCase()}>{_==="Flag" ? "" : _}</th>
                ))}
                </tr>
            </thead>
            <tbody>
                {
                    props.countries.map((country,index) => (
                        <tr key={`${country.name}-${index}`} onClick={()=>handleRowClick(country.name)}>
                            <td>
                                <img src={country.flag} alt={country.name} />
                            </td>
                            <td>
                                {country.population.toLocaleString()}
                            </td>
                            <td>
                                {country.region}
                            </td>
                            <td>
                                {country.capital}
                            </td>
                            <td>
                                {country.name}
                            </td>
                            <td className="currency-td">
                                {
                                    country.currencies.map(c=><span className="currency">
                                        {c.symbol} {c.code}
                                    </span>)
                                }
                            </td>
                        </tr>
                    ))
                }

            </tbody>
        </Table>
    );
};

export default CountryTable;
