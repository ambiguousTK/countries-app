import React, { useEffect } from "react";
import {Alert, Col, Row} from "react-bootstrap/esm/index";
import CountryCard from "../CountryCard/CountryCard";
import {StoreContext} from "../../../App";

const CountryGrid = (props) => {
    const store = React.useContext(StoreContext);
    useEffect(()=>{
        store.setMeta({title: "Countries Card View"});
        return () => {
        store.setMeta({title: ""});

        }
    },[]);

    const countriesGridMarkup = () => {
        const {countries} = props;
        // array of N elements, where N is the number of rows needed
        const rows = [...Array(Math.ceil(countries.length / 4))];
        // chunk the products into the array of rows
        const countriesRows = rows.map((row, idx) => countries.slice(idx * 4, idx * 4 + 4));
        return (
            <div className="countries">
                {countriesRows.map((row, index) => (
                    <Row key={`row-${index}`}>
                        {row.map((country, index) => (
                            <Col key={`country-col-${index}`} sm={12} md={12} lg={3}>
                                <CountryCard country={country}/>
                            </Col>
                        ))}
                    </Row>

                ))
                }
            </div>
        );

    };
    if (props.countries && Array.isArray(props.countries) && props.countries.length > 0)
        return countriesGridMarkup();
    else
        return <Alert variant="danger">No countries</Alert>;




}

export default CountryGrid;
