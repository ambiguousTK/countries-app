import React from "react";
import {StoreContext} from "../../App";
import {useObserver} from "mobx-react";
require("./searchCountries.scss");

const SearchCountries = () => {
    const store = React.useContext(StoreContext);

    const {searchForCountry} = store;
    const handleInputChange = (e) => {
        searchForCountry(e.target.value);
    };

    return useObserver(() => (
        <div className="input-container">
            <div className="search-icon"/>
            <input className="input-field" type="text" value={store.countrySearchValue}
                   onChange={(e) => handleInputChange(e)}
                   placeholder="search for a country" name="countrySearch"/>
        </div>

    ));

};

export default SearchCountries;
