const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');

const wp = require("./wp");
const devServer = {

    disableHostCheck: true,
    headers: {
        "Access-Control-Allow-Origin": "*",
    },
    compress: true,
    inline: true,
    hot: true,
    historyApiFallback: true,
};

module.exports = {
    context: wp.rootPath,
    devServer: devServer,

    output: {
        path: wp.outputPath,
        filename: 'bundle.js',
        publicPath: '/',
    },
    resolve: {
        // modules: [path.join(__dirname, 'src'), 'node_modules'],
        // alias: {
        //     react: path.join(__dirname, 'node_modules', 'react'),
        // },
        modules: [
            wp.srcPath,
            "node_modules",
        ]
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                    },
                ],
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader",
                ],
            },
        ],
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: './public/index.html',
        }),
        new ESLintPlugin(),
    ],
};
