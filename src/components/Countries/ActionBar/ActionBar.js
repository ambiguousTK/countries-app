import React from "react";
import {Col, Row} from "react-bootstrap";
import SearchCountries from "../../SearchCountries/SearchCountries";
import FilterByRegion from "../../FilterByRegion/FilterByRegion";
import SwitchView from "../../SwitchView/SwitchView";

require("./actionBar.scss");

const ActionBar = () => {

    return (
        <div className="action-bar">
            <Row className="mt-2 mb-4">
                <Col lg={4} className="mobile-space mt-sm-4">
                    <SearchCountries/>
                </Col>
                <Col lg={{
                    span: 3,
                    offset: 3
                }}
                    xs={7}
                    className="mobile-space mt-sm-4"
                >
                    <FilterByRegion/>
                </Col>
                <Col lg={2} xs={5} className="mobile-space mt-sm-4">
                    <SwitchView/>
                </Col>
            </Row>
        </div>
    );
};

export default ActionBar;
