import React, {useEffect, useState} from "react";
import {StoreContext} from "../../App";

require("./filterByRegion.scss");

const FilterByRegion = () => {
    const store = React.useContext(StoreContext);

    const regions = ["Africa", "Americas", "Asia", "Europe", "Oceania"];
    const [isOpen, setOpen] = useState(false);
    const [region, setRegion] = useState("");
    const handleRegionClick = (region) => {
        setOpen(false);
        setRegion(region);
        store.searchByRegion(region);
    };
    useEffect(()=>{
        if (store.regionSearchValue != null && store.regionSearchValue.length > 0) {
            setRegion(store.regionSearchValue);
        }
    },[]);
    const handleClearRegion = () => {
        setRegion("");
        store.searchByRegion("");
    };
    return (
        <div className="filter-by-region">
            {region==="" &&
                <button onClick={() => setOpen(!isOpen)}>filter by region
                    <div id="caret-down" className={isOpen ? "open" : "closed"}/>
                </button>
            }
            {
                region!=="" &&
                <div className="region-selected">
                    {region}
                    <div id="clear" onClick={()=>handleClearRegion()}>clear</div>
                </div>

            }
            {isOpen && <div className="options">
                {regions.map(region => {
                    return <div key={region} className="option" onClick={()=>handleRegionClick(region)}>{region}</div>;
                })}
            </div>}

        </div>

    );
};

export default FilterByRegion;
