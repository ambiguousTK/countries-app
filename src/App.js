import React from "react";
import {ThemeProvider} from 'styled-components';
import {lightTheme, darkTheme} from './styles/theme';
import {GlobalStyles} from './styles/global';
import {Route, Switch} from "react-router-dom";
import Countries from "./components/Countries/Countries";
import Header from "./components/Header";
import {useLocalStore, useObserver} from "mobx-react";
import {Container} from "react-bootstrap"
import CountryDetails from "./components/CountryDetails/CountryDetails";
import NotFound from "./components/NotFound/NotFound";

export const StoreContext = React.createContext();

const StoreProvider = ({children}) => {
    const store = useLocalStore(() => ({
        theme: localStorage.getItem("theme") || "light",
        toggleTheme: () => {
            store.theme = store.theme === "light" ? "dark" : "light";
            localStorage.setItem("theme", store.theme);
        },
        countrySearchValue: "",
        searchForCountry: (country) => {
            store.countrySearchValue = country;
        },
        regionSearchValue: "",
        searchByRegion: (region) => {
            store.regionSearchValue = region;
        },
        countriesViewMode: localStorage.getItem("countriesViewMode") || "card",
        setCountriesViewMode: (mode) => {
            if (mode != null) {
                store.countriesViewMode = mode;
                localStorage.setItem("countriesViewMode", store.countriesViewMode);
            }
        },
        meta: {
            title: ""
        },
        setMeta: (meta) => {
            store.meta = {...meta};
        },
    }));
    return useObserver(() => (
        <StoreContext.Provider value={store}>
            <ThemeProvider theme={store.theme === 'light' ? lightTheme : darkTheme}>
                <>
                    <GlobalStyles/>
                    {children}
                </>
            </ThemeProvider>
        </StoreContext.Provider>
    ));
};


function App() {

    return useObserver(() => (
        <StoreProvider>
            <Header/>
            <Container>
                <Switch>
                    <Route path="/" component={Countries} exact/>
                    <Route path="/country/:name" component={CountryDetails} exact/>
                    <Route component={NotFound} />
                </Switch>
            </Container>
        </StoreProvider>
    ));
}

export default App;
