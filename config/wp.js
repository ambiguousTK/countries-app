const path = require("path");
const fs = require("fs");

const webpack = require("webpack");

const wp = {};

wp.rootPath = path.resolve(__dirname, "..");
wp.srcPath = path.resolve(wp.rootPath, "src");
wp.outputPath = path.resolve(wp.rootPath, "build");
// wp.publicOutputPath = path.resolve(wp.outputPath, "client");
wp.templatePath = path.resolve(wp.rootPath, "public");

module.exports = wp;
