import {createGlobalStyle} from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  // *,
  // *::after,
  // *::before {
  //   box-sizing: border-box;
  // }

  body {
    // align-items: center;
    background: ${({theme}) => theme.background};
    color: ${({theme}) => theme.text};
    // display: flex;
    // flex-direction: column;
    // justify-content: center;
    // height: 100vh;
    // margin: 0;
    // padding: 0;
    font-family: BlinkMacSystemFont, -apple-system, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;
    transition: all 0.25s linear;
    header {
      background-color: ${({theme}) => theme.body};
    }
    .card {
      background-color: ${({theme}) => theme.body};
    }

    .card-body {
      background: ${({theme}) => theme.body};
      color: ${({theme}) => theme.text};
    }

    input {
      background: ${({theme}) => theme.body};
      color: ${({theme}) => theme.text};
    }

    .filter-by-region button {
      background: ${({theme}) => theme.body};
      color: ${({theme}) => theme.text};
    }

    .search-icon {
      background-color: ${({theme}) => theme.text};
    }

    #caret-down {
      background-color: ${({theme}) => theme.text};
    }

    #clear {
      background-color: ${({theme}) => theme.text};
    }
    #back-button {
      background-color: ${({theme}) => theme.body};
      color: ${({theme}) => theme.text};
    }
    #left-arrow {
      background-color: ${({theme}) => theme.text};
    }
    .country-boundry {
      background-color: ${({theme}) => theme.body};
    }
    .input-container {
      background-color: ${({theme}) => theme.body};
    }
    .loading-component-container {
      padding-top: 5rem; 
      text-align: center;
      height: 20rem;
    }
    .options {
      background: ${({theme}) => theme.body};
      color: ${({theme}) => theme.text};

      .option:hover {
        background: ${({theme}) => theme.hover};
      }
    }

    input[type="checkbox"].switch + div {
      background: ${({theme}) => theme.body};
      border: 1px solid ${({theme}) => theme.text};
    }

    input[type="checkbox"].switch + div > div {
      background: ${({theme}) => theme.text};
    }
    .table {
      thead {
        background: ${({theme}) => theme.text};
        color: ${({theme}) => theme.body};
      }
      td {
        color: ${({theme}) => theme.text};
        font-weight: 500;
        .currency {
          border-color: ${({theme}) => theme.hover};
        }
      }
    }

  }

  ::-webkit-input-placeholder {
    color: ${({theme}) => theme.text};
    opacity: 0.5;
  }

  ::-moz-placeholder {
    color: ${({theme}) => theme.text};
    opacity: 0.5;
  }

  :-ms-input-placeholder {
    color: ${({theme}) => theme.text};
    opacity: 0.5;
  }

  :-moz-placeholder {
    color: ${({theme}) => theme.text};
    opacity: 0.5;
  }

  body::-webkit-scrollbar {
    width: 0.8em;
  }

  body::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px ${({theme}) => theme.hover};
  }

  body::-webkit-scrollbar-thumb {
    background-color: ${({theme}) => theme.hover};
    outline: 1px solid ${({theme}) => theme.body};
    border-radius: 12px;
  }`;
